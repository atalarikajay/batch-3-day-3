import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  Alert,
} from 'react-native';
import React, {useState} from 'react';

const App = () => {
  const [items, setItems] = useState('');
  const ambil = () => {
    Alert.alert('Berhasil Menyimpan Data');
    var myHeaders = new Headers(); //header untuk api atau ngirim data ke backEnd
    myHeaders.append('Content-Type', 'application/json');

    var raw = JSON.stringify({
      name: items,
    });

    var requestOptions = {
      method: 'PATCH', // bisa diganti get,post,delete,patch,put,option
      headers: myHeaders, //ini adalah untuk ngirim data ke backEnd ke header/kepalanya
      body: raw, // isi data yang akan di kirim
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users/-NMalhNh5LPYMCq7uuUh.json',
      requestOptions,
    )
      //maksud dari then adalah ketika api sudah di jalan maka ambil datanya
      .then(response =>
        // kita convert ke text dulu
        response.text(),
      )
      .then(result => {
        // isi yang sudah di convert dari si text()
        console.log(result), setItems(result);
      })
      //catch adalah untuk menangkap data yang error
      .catch(error => console.log('error', error));
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#3EB489'}}>
      <View style={{marginTop: 300}}>
        <TextInput
          placeholder="Masukkan Data"
          onChangeText={items => setItems(items)}
          style={{
            backgroundColor: '#ffffff',
            marginHorizontal: 75,
            borderRadius: 5,
            paddingVertical: 10,
            elevation: 5,
            textAlign: 'center',
          }}
          selectionColor={'cyan'}></TextInput>
        <TouchableOpacity
          style={{
            marginHorizontal: 115,
            marginTop: 20,
            backgroundColor: 'cyan',
            paddingVertical: 10,
            borderRadius: 5,
            elevation: 5,
          }}
          onPress={() => {
            ambil();
          }}>
          <Text style={{textAlign: 'center', fontWeight: 'bold'}}>SUBMIT</Text>
        </TouchableOpacity>
        <TextInput
          editable={false}
          style={{
            color: 'red',
            marginHorizontal: 90,
            marginTop: 20,
            backgroundColor: '#ffffff',
            paddingVertical: 10,
            borderRadius: 5,
            textAlign: 'center',
          }}>
          {items}
        </TextInput>
      </View>
    </SafeAreaView>
  );
};

export default App;
